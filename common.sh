#!/bin/bash

DATE=$(which date)

function log_error {
  if [ -n "$DATE" ]; then
    echo -e "[$($DATE '+%F %H:%M')] $1" 1>&2
  else
    echo -e "$1" 1>&2
  fi
}

function log_info {
  if [ -n "$DATE" ]; then
    echo -e "[$($DATE '+%F %H:%M')] $1"
  else
    echo -e "$1"
  fi
}

function die {
  echo -e "$1" 1>&2
  exit 1
}

function die_directory_not_found {
  die "directory not found: $1"
}

function die_file_not_found {
  die "file not found: $1"
}

function die_command_not_found {
  die "command not found: $1"
}

# usage: check-not-null "function-foobar" "$1" "username"
function check-not-empty {
  test -n "$2" || die "function '$1' requires parameter: '$3'"
}

# $1 - message
function ask_yes_no {
  echo -n "$1 [yes/no]: "
  while true; do
    read answer
    if [ "$answer" = "yes" ]; then
      return 0
    elif [ "$answer" = "no" ]; then
      return 1
    else
      echo -e -n "\nUnable to understand your answer. Please reply with 'yes' or 'no': "
    fi
  done
}
